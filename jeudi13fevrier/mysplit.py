import unittest


def mySplit(string, separator):
    if string == None or string != str or separator == None:
        return "Enter the required parameters"
    strings = []
    current_string = ""
    for char in string:
        if char == separator:
            if current_string:
                strings.append(current_string)
                current_string = ""
        else:
            current_string += char
    if current_string:
        strings.append(current_string)
    return strings


class TestStringMethods(unittest.TestCase):

    def testInputSeparator(self):
        self.assertEqual(mySplit('hello world', None), 'Enter the required parameters')

    def testInputString(self):
        self.assertEqual(mySplit(None, " "), 'Enter the required parameters')

    def testAllInputs(self):
        self.assertEqual(mySplit(None, None), 'Enter the required parameters')

    def testSeparator(self):
        self.assertEqual(mySplit('hello world', " "), ['hello', 'world'])

    def testDoubleSeparator(self):
        self.assertEqual(mySplit('hello  world', " "), ['hello', 'world'])

    def testTypeOutput(self):
        self.assertTrue(type(mySplit('hello world', " ")), list)

    def testLengthOutput(self):
        self.assertEqual(len(mySplit('hello world', " ")), 2)


if __name__ == '__main__':
    unittest.main()    