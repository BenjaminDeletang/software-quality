def getNumber(string):
    v = 0
    for i in string:
        u = ord(i) - 48
        v = v * 10 + u
    return v


print(getNumber("123456789"))
print(type(getNumber("123456789")))


def reversGetNumber(inte):
    x = ""
    while inte != 0:
        w = chr(inte % 10 + 48)
        x = x + w
        inte  = int(inte/10)
    return x[::-1]


print(reversGetNumber(123456789))
print(type(reversGetNumber(123456789)))
