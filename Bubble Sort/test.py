import time

TimeTime = time.time()

def bubbleSort(arr):
    n = len(arr)

    for i in range(n):

        for j in range(0, n - i - 1):

            if int(arr[j]) > int(arr[j + 1]):
                arr[j], arr[j + 1] = arr[j + 1], arr[j]


file = open("toto.txt", "r")
arr = file.read().split()

bubbleSort(arr)

for i in range(len(arr)):
    print("%s" % arr[i]),

ecart = time.time() - TimeTime

print(ecart)