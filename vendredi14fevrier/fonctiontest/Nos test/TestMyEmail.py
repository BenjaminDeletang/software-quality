import unittest

def myMailVerif(string):
    tableMail = ['jean.michel@gmail.com', 'lee.belule@orange.fr', 'serpent.affamer@myspace.org']
    return "Your mail adress is valid"

class TestStringMethods(unittest.TestCase):


    def testFunctionGood(self):
        input = "jean.michel@gmail.com"
        self.assertEqual(myMailVerif(input), "It's an email" )

    
    def testAtInString(self):
        input = "jean.michelgmail.com"
        self.assertEqual(myMailVerif(input), "It's not an email" )


    def testDotEndOfString(self):
        input = "jean.michel@gmailcom"
        self.assertEqual(myMailVerif(input), "It's not an email" )


    def testSpecialCharacter(self):
        input = "barnabé&duplo.company@gmail.com"
        self.assertEqual(myMailVerif(input), "It's not an email" )

    
    def testTypeInput(self):
        input = "jean.michel@gmailcom"
        self.assertTrue(type(input), str)

    
    def testIfExist(self):
        input = "jean.michel@gmailcom"
        self.assertTrue(myMailVerif(input), "This email exist" )



if __name__ == '__main__':
    unittest.main()    